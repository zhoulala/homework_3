/*
 Navicat MySQL Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 50715
 Source Host           : localhost
 Source Database       : fingerprint

 Target Server Type    : MySQL
 Target Server Version : 50715
 File Encoding         : utf-8

 Date: 11/16/2016 11:17:28 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `song`
-- ----------------------------
DROP TABLE IF EXISTS `song`;
CREATE TABLE `song` (
  `song_id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`song_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `song_finger`
-- ----------------------------
DROP TABLE IF EXISTS `song_finger`;
CREATE TABLE `song_finger` (
  `finger_id` int(11) DEFAULT NULL,
  `song_id` int(11) DEFAULT NULL,
  `offset` int(11) DEFAULT NULL,
  KEY `finger_id_index` (`finger_id`),
  KEY `finger_index` (`finger_id`),
  KEY `fingerIndex` (`finger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
