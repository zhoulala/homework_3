package fingerprint;

/**
 * Created by guolanqing on 16/11/3.
 */
public class ShazamHash {
    public short f1;
    public short f2;
    public short dt;
    public int finger_id;
    public int offset;
    public int id;
}