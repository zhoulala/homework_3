package scanner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alison on 2016/10/24.
 */
public class ScannerTest {

    public static void main(String[] args) {

        //读取时域数据对象
        WaveFileReader reader = new WaveFileReader("data/wav_part_3/周杰伦 - 青花瓷.wav");
        //输出结果Double[]数组
        double[] wholeResult = reader.getFinalData();  //也就是最终结果，做到这一步就够了，打印出来是为了和MATLAB比较波形
        double[] sequence = wholeResult;

        System.out.println("数组长度：" + sequence.length);

        //将sequence[]写入test.txt
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileWriter(new File("data/text_files/test.csv")));
            out.print("[ ");
            for (int i = 0; i < sequence.length; ++i) {
                out.printf("%.2f ", sequence[i]);
            }
            out.println("];");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //读取一个文件夹下所有文件及子文件夹下的所有文件
    public static void ReadAllFile(String filePath) {
        File f = null;
        f = new File(filePath);
        File[] files = f.listFiles(); // 得到f文件夹下面的所有文件。
        List<File> list = new ArrayList<File>();
        for (File file : files) {
            if (file.isDirectory()) {
                //如何当前路劲是文件夹，则循环读取这个文件夹下的所有文件
                ReadAllFile(file.getAbsolutePath());
            } else {
                list.add(file);
            }
        }
        for (File file : files) {
            System.out.println(file.getAbsolutePath());
        }
    }

}
