package grade;

import fingerprint.ShazamHash;

import java.sql.*;
import java.util.ArrayList;

import static database.StoreFingerprint.password;
import static database.StoreFingerprint.url;
import static database.StoreFingerprint.user;

/**
 * Created by Alison on 2016/11/19.
 */
public class Grade {
    //查询数据库寻找匹配程度最高的五首歌曲
    public static void Grade(ArrayList<ShazamHash> m_hashes, ArrayList<ArrayList<Integer>> data) {

        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        PreparedStatement pstm = null;

        try {
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();

            //构造sql语句

            String sql = "SELECT song_id,offset FROM song_finger WHERE finger_id=?";


            PreparedStatement ps = con.prepareStatement(sql);


            for (int i = 0; i < m_hashes.size(); i++) {

                int offset = m_hashes.get(i).offset;
                ps.setInt(1, m_hashes.get(i).finger_id);

                rs = ps.executeQuery();
                while (rs.next()) {
                    int id = rs.getInt("song_id");
                    int minus = rs.getInt("offset") - offset;
                    data.get(id - 1).add(minus);
                }
            }
            st.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
