package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static database.StoreFingerprint.password;
import static database.StoreFingerprint.url;
import static database.StoreFingerprint.user;

/**
 * Created by Alison on 2016/11/19.
 */
public class Print {

    public static List<String> PrintResult(List<Integer> list) {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List<String> songs = new ArrayList<String>();

        try {
            con = DriverManager.getConnection(url, user, password);
            //构造sql语句
            String sql = "SELECT name FROM song WHERE song_id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            for (int i = 0; i < 5; i++) {
                ps.setInt(1, list.get(i));
                rs = ps.executeQuery();
                /*
                 * there was an error, but it has been fixed now
                 * when you already prepared the statement in connection.prepareStatement(query);
                 * then why to pass the query again in selectUser.executeQuery(query);
                 */
                if (rs.next()) {
                    songs.add(rs.getString("name"));
                }
                rs.close();
            }

            System.out.println("We found five possible songs for you：" + "\n");
            int i = 0;
            for (String name : songs) {
                System.out.println("No." + (++i) + "：  " + name);
            }
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return songs;
    }
}
